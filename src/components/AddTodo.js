import React, {useState} from "react";

const AddTodo = ({ add }) => {
  const [title, setTitle] = useState("")

  const change = e => {
    setTitle(e.target.value)
  }

  const submit = e => {
    e.preventDefault();
    add(title)
  }

  return(
    <form onSubmit={submit}>
      <input type="text" value={title} onChange={change} />  
      <button>add</button>
    </form>
  )
}

export default AddTodo;

