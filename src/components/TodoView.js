import React from 'react';
import'../styles/TodoView.css';
import { connect } from 'react-redux'

const TodoView = ({title, lists}) => {
  return(
  <div>
    <h1>{title}</h1>  
    {lists.map(item =>
      <div key={item.id}>
        <p>{item.title}</p>
      </div>
    )}
  </div>
  )
}


/*const TodoView = ({todos, children, delTask})=> {
    return (
      <div>
          {todos.map ( item =>
            <div key={item.id}>
            <h2 className= {item.completed? "merah":"biru"}>{item.title}</h2>
            <button onClick={()=> delTask(item.id)}>del</button>
            </div>
          )}
      </div>
    )
  }
*/
const mapStateToProps = state =>({
  title: state.todoReducer.title,
  lists: state.todoReducer.todos
})

export default connect(mapStateToProps)(TodoView);
