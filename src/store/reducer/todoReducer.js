const initialState = {
    todos: [
      {
        id : 1,
        title : "delectus",
        completed: false
      },
      {
        id: 2,
        title : "quis",
        completed :true
      }
    ],
    title:'judul'
  }

const todoReducer = (state= initialState, action)=>{
  switch (action.type){
    default:
      return {
        ...state
      }
  }
}

export default todoReducer;
